package com.perfectomobile.integrations.almselenium;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ReportTestResult {

	Integer NewRunId;
	
	public ReportTestResult() {
		// TODO Auto-generated constructor stub
		NewRunId = 0;
	}

	public void Execute(String ServerUrl, String Username, String Password,
			String Domain, String Project, int TestConfigId, int TestSetId,
			String ExecutionStatus) {

		try {
			String line;
			String command;

			command = "cmd /c \"\"" + System.getProperty("user.dir")
					+ "\\lib\\ALMSeleniumBridge.exe\"";
			command = command + " -operation=report -url=" + ServerUrl
					+ " -username=" + Username + " -password=" + Password
					+ " -domain=" + Domain + " -project=" + Project
					+ " -config=" + TestConfigId + " -testset=" + TestSetId
					+ " -status=" + ExecutionStatus + "\"";

			System.out.println(command);

			Process p = Runtime.getRuntime().exec(command);
			BufferedReader bri = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));
			while ((line = bri.readLine()) != null) {
				System.out.println(line);
				NewRunId = Integer.parseInt(line);
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();
			p.waitFor();
		} catch (Exception err) {
			err.printStackTrace();
		}

	}
	
	public Integer GetCreatedRunId() 
	{
		return NewRunId;
	}

}
