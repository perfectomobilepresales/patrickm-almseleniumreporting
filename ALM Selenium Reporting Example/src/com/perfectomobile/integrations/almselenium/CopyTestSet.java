package com.perfectomobile.integrations.almselenium;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CopyTestSet {

	Integer NewTestSetId;

	public void Execute(String ServerUrl, String Username, String Password,
			String Domain, String Project, int SourceTestSetId,
			String DestinationFolderPath, String NewTestSetName) {

		try {
			String line;
			String command;

			command = "cmd /c \"\"" + System.getProperty("user.dir")
					+ "\\lib\\ALMSeleniumBridge.exe\"";
			command = command + " -operation=copy -url=" + ServerUrl
					+ " \"-username=" + Username + "\" -password=" + Password
					+ " -domain=" + Domain + " -project=" + Project
					+ " -template=" + SourceTestSetId + " \"-destination="
					+ DestinationFolderPath + "\" \"-name=" + NewTestSetName
					+ "\"\"";

			System.out.println(command);

			Process p = Runtime.getRuntime().exec(command);
			BufferedReader bri = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));
			while ((line = bri.readLine()) != null) {
				System.out.println(line);
				NewTestSetId = Integer.parseInt(line);
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();
			p.waitFor();
		} catch (Exception err) {
			err.printStackTrace();
		}

	}

	public CopyTestSet() {
		NewTestSetId = 0;

	}

	public Integer GetCreatedTestSetId() {
		return NewTestSetId;
	}

}
