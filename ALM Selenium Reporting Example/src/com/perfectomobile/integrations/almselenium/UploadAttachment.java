package com.perfectomobile.integrations.almselenium;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class UploadAttachment {

	public UploadAttachment() {
		// TODO Auto-generated constructor stub
	}
	
	public void Execute(String ServerUrl, String Username, String Password,
			String Domain, String Project, String EntityType, int EntityId, String AttachmentPath,
			String AttachmentType) {

		try {
			String line;
			String command;

			command = "cmd /c \"\"" + System.getProperty("user.dir")
					+ "\\lib\\ALMSeleniumBridge.exe\"";
			
			switch (EntityType) 
			{
				case "Test Set": 
					command = command + " -operation=uploadtsattachment -url=" + ServerUrl
					+ " -username=" + Username + " -password=" + Password
					+ " -domain=" + Domain + " -project=" + Project
					+ " -testsetid=" + EntityId + " \"-attachment=" + AttachmentPath + "\""
					+ " -attachtype=" + AttachmentType + "\"";
					break;
				case "Run":
					command = command + " -operation=uploadrunattachment -url=" + ServerUrl
					+ " -username=" + Username + " -password=" + Password
					+ " -domain=" + Domain + " -project=" + Project
					+ " -runid=" + EntityId + " \"-attachment=" + AttachmentPath + "\""
					+ " -attachtype=" + AttachmentType + "\"";
					break;
			}

			System.out.println(command);

			Process p = Runtime.getRuntime().exec(command);
			BufferedReader bri = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));
			while ((line = bri.readLine()) != null) {
				System.out.println(line);

			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();
			p.waitFor();
		} catch (Exception err) {
			err.printStackTrace();
		}

	}


}
