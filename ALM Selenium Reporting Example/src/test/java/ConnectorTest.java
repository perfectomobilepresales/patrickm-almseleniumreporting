package test.java;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeSuite;

import com.perfectomobile.integrations.almselenium.CopyTestSet;
import com.perfectomobile.integrations.almselenium.ReportTestResult;
import com.perfectomobile.integrations.almselenium.UploadAttachment;

public class ConnectorTest {

	Integer NewTestSetId;
	String BuildNumber = "Build #" + System.getProperty("BuildNumber");
	String AlmUrl = System.getProperty("AlmUrl");
	String Username = System.getProperty("Username");
	String Password = System.getProperty("Password");

	@Test(dataProvider = "dp")
	public void f(Integer n, String name, Integer TestConfigId, String Status) {
		ReportTestResult reporter;
		reporter = new ReportTestResult();
		reporter.Execute(AlmUrl, Username, Password, "DEFAULT",
				"Selenium_Reporting", TestConfigId, NewTestSetId, Status);
		
		UploadAttachment uploader; 
		uploader = new UploadAttachment();
		
		String attachmentPath = System.getProperty("user.dir") + "\\lib\\testrun.txt";
		uploader.Execute(AlmUrl, Username, Password, "DEFAULT", "Selenium_Reporting", "Run", reporter.GetCreatedRunId(), attachmentPath, "File");

	}

	@DataProvider
	public Object[][] dp() {
		return new Object[][] { new Object[] { 1, "IE8", 1002, "Passed" },
				new Object[] { 2, "IE9", 1003, "Failed" },
				new Object[] { 3, "FF34", 1004, "Passed" } };
	}

	@BeforeSuite
	public void beforeSuite() {
		CopyTestSet copy;
		copy = new CopyTestSet();
		copy.Execute(AlmUrl, Username, Password, "DEFAULT",
				"Selenium_Reporting", 1, "Root@Builds", BuildNumber);
		NewTestSetId = copy.GetCreatedTestSetId();
	}

	@AfterSuite
	public void afterSuite() {
		UploadAttachment uploader;
		uploader = new UploadAttachment();
		
		String attachmentPath = System.getProperty("user.dir") + "\\lib\\testset.txt";
		uploader.Execute(AlmUrl, Username, Password, "DEFAULT", "Selenium_Reporting", "Test Set", NewTestSetId, attachmentPath, "File");
		uploader.Execute(AlmUrl, Username, Password, "DEFAULT", "Selenium_Reporting", "Test Set", NewTestSetId, "http://www.google.com", "URL");

	}
}
